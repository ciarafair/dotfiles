#██████   ██████  ████████ ███████ ██ ██      ███████ ███████
#██   ██ ██    ██    ██    ██      ██ ██      ██      ██
#██   ██ ██    ██    ██    █████   ██ ██      █████   ███████
#██   ██ ██    ██    ██    ██      ██ ██      ██           ██
#██████   ██████     ██    ██      ██ ███████ ███████ ███████

echo "source $HOME/.config/.zshrc" >$HOME/.zshrc
echo "source $HOME/.config/.bashrc" >$HOME/.bashrc
echo "source $HOME/.config/tmux/tmux.conf" >$HOME/.tmux.conf

